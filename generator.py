import json
import sys
import os
from stress_test_generator import generate_code_main_flow
import logging
from jinja2 import Template
import json

logger = logging.getLogger("")
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.DEBUG)

def generate_test_code_from_file(
    mainFile='main.py',
    dockerComposeFile='compiled_docker_compose.yml',
    credentials="bunnyshell:admin"
):
    if len(sys.argv) < 1:
        raise ValueError("Please provide file path with json data to generate")

    file_path = sys.argv[1]
    if not os.path.isfile(file_path):
        raise ValueError("The file path %s does not exists" % file_path)

    with open(file_path) as json_file:
        stress_map = json.load(json_file)
        logging.info("Generate stress test based on file %s" % file_path)
        generate_test_code_from_map(stress_map=stress_map, dockerComposeFile=dockerComposeFile)

def generate_test_code_from_map(
    stress_map,
    mainFile='main.py',
    dockerComposeFile='compiled_docker_compose.yml',
    credentials="bunnyshell:admin"
):
    self_main = "main_self_execute.py"

    # cleanup files
    if os.path.isfile(mainFile):
        os.remove(mainFile)

    # cleanup files
    if os.path.isfile(self_main):
        os.remove(self_main)

    if os.path.isfile(dockerComposeFile):
        os.remove(dockerComposeFile)

    # generate new files

    flow_code = generate_code_main_flow(logger=logger, stress_test_map=stress_map).generate(stress_map=stress_map)

    logging.info("------------------------------")
    logging.info("Generated flow is:\n %s" % flow_code)
    logging.info("------------------------------")
    logging.info("Preparing writing to main.py...")

    main_template_file = open("templates/main.py.template","r")
    main_template_file_content = main_template_file.read()

    self_main_template_file = open("templates/self_execute.j2","r")
    self_main_template_file_content = self_main_template_file.read()

    main_template = Template(main_template_file_content)
    self_template = Template(self_main_template_file_content)

    fixtures = {}
    if "fixtures" in stress_map.keys():
        fixtures = "fixtures = " + json.dumps(stress_map['fixtures'], indent=2) + "\n"

    # final result code
    result_code = main_template.render(mainFlowClass=flow_code, mainClassName="MainFlow", fixtures=fixtures)
    # flow_code = "\n    ".join(flow_code.split("\n"))
    self_result_code = self_template.render(mainFlowClass=flow_code, mainClassName="MainFlow")

    logging.info("----------------  GENERATED CODE BEGIN -------------")
    logging.info(result_code)
    logging.info("----------------  GENERATED CODE  END  -------------")

    with open(mainFile, 'w') as f:
        f.write(result_code)

    logging.info("----------------  GENERATED CODE BEGIN -------------")
    logging.info(self_result_code)
    logging.info("----------------  GENERATED CODE  END  -------------")

    with open(self_main, 'w') as f:
        f.write(self_result_code)

    logging.info("Test generated")

    logging.info("Generating docker-compose file...")
    docker_file_template = open("templates/docker-compose.yml.j2","r")
    docker_file_template_content = docker_file_template.read()
    docker_file_template_compiled = Template(docker_file_template_content)

    # we support hosts as ip and as dict
    app_hosts = {}
    if "hosts" in stress_map['header']:
        app_hosts = stress_map['header']['hosts']

    hosts = {
        "private_host": "127.0.0.1"
    }
    if isinstance(app_hosts, list):
        for hostConfig in app_hosts:
            print(str(hostConfig))
            hosts[hostConfig["name"]] = hostConfig["ip"]

    if isinstance(app_hosts, dict):
        for hostName, hostIp in app_hosts.items():
            hosts[hostName] = hostIp
    docker_compose_result = docker_file_template_compiled.render(
        hostname=stress_map["header"]["schema"] + "://" + stress_map["header"]["domain"],
        domain=stress_map["header"]["domain"],
        hosts=hosts
    )

    with open(dockerComposeFile, 'w') as f:
        f.write(docker_compose_result)

    logging.info("Generating environemnt file...")
    env_file_template = open("templates/.env_template.j2","r")
    env_file_template_content = env_file_template.read()
    env_file_template_compiled = Template(env_file_template_content)
    dot_env_content = env_file_template_compiled.render(
        hostname=stress_map["header"]["schema"] + "://" + stress_map["header"]["domain"],
        domain=stress_map["header"]["domain"],
        credentials=credentials
    )

    with open('.env', 'w') as f:
        f.write(dot_env_content)

if __name__ == "__main__":
    # usage in cli
    generate_test_code_from_file(mainFile='main.py', dockerComposeFile='compiled_docker_compose.yml', credentials="bunnyshell:admin")