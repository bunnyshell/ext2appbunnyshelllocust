
function install_python() {
    echo "Install Virtual Env..."

    # virtualenv
    apt-get install -y virtualenv

    # python packages
    apt-get install -y python3-pip
    apt-get install -y python-pip
    apt-get install git

    # clone project
    virtualenv -p python3 venv

    echo "Install requiremens..."

    venv/bin/pip install -r requirements.txt
    source venv/bin/activate

    echo "Next in line:"
    echo "  1. write your test json file"
    echo "  2. generate test files from json (json as testing):"
    echo  "     python generator.py <path to json>"
    echo "  3. execute command which starts / restarts machines"
    echo "      bash ./bin/restart_docker.sh `pwd`/compiled_docker_compose.yml `nproc --all`"
}

install_python