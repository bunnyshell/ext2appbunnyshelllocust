import FuseAnimate from '@fuse/core/FuseAnimate';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { darken } from '@material-ui/core/styles/colorManipulator';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import curlCompiler from './CurlCompiler';

const AntTabs = withStyles({
	root: {
		borderBottom: '1px solid #e8e8e8'
	},
	indicator: {
		backgroundColor: '#1890ff'
	}
})(Tabs);

const useStyles = makeStyles(theme => ({
	root: {
		'& .MuiTextField-root': {
			margin: theme.spacing(1),
			width: '50ch'
		}
	}
}));

function Request(props) {
	const classes = useStyles();

	return (
		<div>
			<h1>Request</h1>
		</div>
	)
}

export default Request;
