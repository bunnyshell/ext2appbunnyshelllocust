import FuseAnimate from '@fuse/core/FuseAnimate';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { darken } from '@material-ui/core/styles/colorManipulator';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { FixedSizeList } from 'react-window';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import axios from 'axios';
import SimpleModal from '../documentation/material-ui-components/components/modal/SimpleModal';
import curlCompiler from './CurlCompiler';

function getModalStyle() {
	const top = 50;
	const left = 50;

	return {
		top: `${top}%`,
		left: `${left}%`,
		width: '60%',
		height: '60%',
		transform: `translate(-${top}%, -${left}%)`
	};
}

const useStyles = makeStyles(theme => ({
	root: {
		'& .MuiTextField-root': {
			margin: theme.spacing(1),
			width: '50ch'
		}
	},
	paper: {
		position: 'absolute',
		width: 400,
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3)
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120
	},
	selectEmpty: {
		marginTop: theme.spacing(2)
	}
}));

function StressTesting(props) {
	const [modalStyle] = React.useState(getModalStyle);
	const [open, setOpen] = React.useState(false);

	const VIEW_TYPE_MAIN = 'main';
	const VIEW_TYPE_REQUESTS = 'requests';
	const VIEW_USER_BEHAVIOUR = 'user_behaviour';
	const VIEW_RUN_TEST = 'run_test';

	const classes = useStyles();
	const [selectedRequest, setSelectedRequest] = React.useState(null);
	const [viewType, setViewType] = React.useState('main');
	const [testJson, setTestJson] = React.useState({});
	const [runTestMessage, setRunTestMessage] = React.useState({ status: 'inactive_test' });

	const handleClose = () => {
		setOpen(false);
	};

	const getDefaultStressTest = () => {
		return {
			header: {
				schema: 'https',
				domain: '',
				hosts: [
					{
						name: 'localhost',
						ip: '127.0.0.1'
					}
				]
			},
			requests: {},
			users: {
				user_basic: [
					{
						type: 'parallel',
						name: 'MainApplicationTasks',
						tasks: []
					}
				]
			}
		};
	};

	const handleRequestSelectionClick = (event, index) => {
		setSelectedRequest(index);
		setViewType(VIEW_TYPE_REQUESTS);
	};

	const [stressTest, setStressTest] = React.useState(getDefaultStressTest());
	const [newTestName, setNewTestName] = React.useState('');

	const handleOpen = () => {
		setTestJson(buildTestJson({ ...stressTest }));
		setOpen(true);
	};

	useEffect(() => {
		let stressTestStorage = localStorage.getItem('stress_test');
		if (!stressTestStorage) {
			stressTestStorage = getDefaultStressTest();
		} else {
			stressTestStorage = JSON.parse(stressTestStorage);
		}
		setStressTest(stressTestStorage);

		console.log('stress test', stressTestStorage);
	}, [props]);

	const saveStressTest = () => {
		localStorage.setItem('stress_test', JSON.stringify(stressTest));
	};

	const compileCurl = value => {
		let compiled = curlCompiler(value);
		compiled = replaceKey(compiled, 'method', 'type');
		compiled = replaceKey(compiled, 'header', 'headers');
		compiled = replaceKey(compiled, 'data', 'form-data');
		compiled.type = compiled.type.toLowerCase();

		return compiled;
	};

	const setSchema = schema => {
		setStressTest({
			...stressTest,
			header: {
				...stressTest.header,
				schema
			}
		});
		saveStressTest();
	};

	const setDomain = domain => {
		setStressTest({
			...stressTest,
			header: {
				...stressTest.header,
				domain
			}
		});
		saveStressTest();
	};

	const replaceKey = (obj, oldKey, newKey) => {
		obj[newKey] = obj[oldKey];
		delete obj[oldKey];
		return obj;
	};

	const newStressTest = name => {
		setNewTestName('');
		stressTest.users.user_basic[0].tasks.push({
			percent: '20',
			request: name
		});

		setStressTest({
			...stressTest,
			requests: {
				...stressTest.requests,
				[name]: getStressTestStructure(name)
			}
		});
		saveStressTest();
		setSelectedRequest(name);
		setViewType(VIEW_TYPE_REQUESTS);
	};

	const getStressTestStructure = name => {
		return {
			name,
			curl: '',
			json_spec: '',
			post_requests: [],
			verify: 0,
			timeout: 30,
			post_variables: [],
			pre_request_requests: [],
			'form-binary': {},
			'form-data': {},
			'x-www-form-urlencoded': {},
			raw_json: {},
			raw_text: {},
			raw_javascript: {},
			raw_html: {},
			raw_xml: {}
		};
	};

	const newPostRequest = requestName => {
		setStressTest({
			...stressTest,
			requests: {
				...stressTest.requests,
				[requestName]: {
					...stressTest.requests[requestName],
					post_requests: [
						...stressTest.requests[requestName].post_requests,
						{
							request: '',
							pre_request_script: ''
						}
					]
				}
			}
		});
		saveStressTest();
	};

	const onChangeRequestName = (keyName, newName) => {
		setStressTest({
			...stressTest,
			requests: {
				...stressTest.requests,
				[keyName]: {
					...stressTest.requests[keyName],
					name: newName
				}
			}
		});
		saveStressTest();
	};

	const onChangeRequestCurl = (keyName, curl) => {
		setStressTest({
			...stressTest,
			requests: {
				...stressTest.requests,
				[keyName]: {
					...stressTest.requests[keyName],
					curl,
					json_spec: compileCurl(curl)
				}
			}
		});
		saveStressTest();
	};

	const setEtcHostName = (hostname, index) => {
		const newTest = stressTest;
		newTest.header.hosts[index].name = hostname;
		setStressTest({ ...newTest });
		saveStressTest();
	};

	const setEtcHostIp = (hostip, index) => {
		const newTest = stressTest;
		newTest.header.hosts[index].ip = hostip;
		setStressTest({ ...newTest });
		saveStressTest();
	};

	const addHostEntry = () => {
		setStressTest({
			...stressTest,
			header: {
				...stressTest.header,
				hosts: [
					...stressTest.header.hosts,
					{
						name: '',
						ip: ''
					}
				]
			}
		});
		saveStressTest();
	};

	const removeEtcHost = indexDel => {
		const newTest = stressTest;
		newTest.header.hosts = newTest.header.hosts.filter(function (value, index, arr) {
			return index !== indexDel;
		});

		setStressTest({ ...newTest });
		saveStressTest();
	};

	const buildTestJson = stressTestArg => {
		const buildObject = JSON.parse(JSON.stringify(stressTestArg));

		Object.keys(buildObject.requests).map(requestName => {
			buildObject.requests[requestName] = {
				...buildObject.requests[requestName],
				...buildObject.requests[requestName].json_spec,
				name: requestName
			};
			delete buildObject.requests[requestName].json_spec;
		});

		return buildObject;
	};

	const addPostRequest = requestName => {};

	const changeBehaviourRequestPercent = (percent, requestIndex) => {
		const userBasic = stressTest.users.user_basic;
		userBasic[0].tasks[requestIndex].percent = percent;
		setStressTest({
			...stressTest,
			users: {
				...stressTest.users,
				user_basic: [...userBasic]
			}
		});
	};

	const changePostRequestName = (value, postRuestIndex) => {
		stressTest.requests[selectedRequest].post_requests[postRuestIndex].request = value;

		setStressTest({
			...stressTest,
			requests: {
				...stressTest.requests,
				[selectedRequest]: {
					...stressTest.requests[selectedRequest],
					post_requests: [...stressTest.requests[selectedRequest].post_requests]
				}
			}
		});
		saveStressTest();
	};

	const changePostRequestCode = (value, postRuestIndex) => {
		stressTest.requests[selectedRequest].post_requests[postRuestIndex].pre_request_script = value;

		setStressTest({
			...stressTest,
			requests: {
				...stressTest.requests,
				[selectedRequest]: {
					...stressTest.requests[selectedRequest],
					post_requests: [...stressTest.requests[selectedRequest].post_requests]
				}
			}
		});
		saveStressTest();
	};

	const runTest = () => {
		setViewType(VIEW_RUN_TEST);
		setRunTestMessage({ status: 'building_test' });
		const testJson = buildTestJson(stressTest);
		axios
			.post(
				'http://0.0.0.0:8088/run_test',
				{
					test: testJson
				},
				{
					timeout: 60000
				}
			)
			.then(res => {
				console.log('Run test response: ', res);
				setRunTestMessage(res.data);
			})
			.catch(err => {
				console.log('Run test err response: ', err);
				setRunTestMessage(err);
			});
	};

	const body = (
		<div style={modalStyle} className={classes.paper}>
			<h2 id="simple-modal-title">Stress Test Json</h2>
			<TextField
				id="outlined-multiline-static"
				label="Bunnyshell Stress Testing Json"
				multiline
				rows={30}
				variant="outlined"
				fullWidth
				value={JSON.stringify(testJson, null, 4)}
			/>
		</div>
	);

	return (
		<>
			<Grid container spacing={3}>
				<Grid item xs={3} sm={3} lx={3} lg={3} md={3} style={{ padding: '20px' }}>
					<Typography variant="h6">Main settings</Typography>
					<ListItem
						button
						selected={viewType === VIEW_TYPE_MAIN}
						onClick={event => setViewType(VIEW_TYPE_MAIN)}
					>
						<ListItemIcon>
							<InboxIcon />
						</ListItemIcon>
						<ListItemText primary="Main Settings" />
					</ListItem>
					<Divider />

					<Typography variant="h6" style={{ marginTop: '50px' }}>
						Requests
					</Typography>
					<Divider />
					<List component="nav" aria-label="main mailbox folders">
						{Object.keys(stressTest.requests).map(key => {
							return (
								<ListItem
									key={key}
									button
									selected={viewType === VIEW_TYPE_REQUESTS && selectedRequest === key}
									onClick={event => handleRequestSelectionClick(event, key)}
								>
									<ListItemIcon>
										<InboxIcon />
									</ListItemIcon>
									<ListItemText primary={stressTest.requests[key].name} />
								</ListItem>
							);
						})}
					</List>

					{/* <Divider /> */}
					<TextField
						label="New Test"
						value={newTestName}
						onChange={ev => setNewTestName(ev.target.value)}
						fullWidth
						onKeyPress={ev => {
							if (ev.key === 'Enter') {
								newStressTest(newTestName);
							}
						}}
					/>
					<Divider />
					<Typography variant="h6" style={{ marginTop: '50px' }}>
						Users Behaviour
					</Typography>
					<List component="nav" aria-label="main mailbox folders">
						<ListItem
							button
							selected={viewType === VIEW_USER_BEHAVIOUR}
							onClick={event => setViewType(VIEW_USER_BEHAVIOUR)}
						>
							<ListItemIcon>
								<InboxIcon />
							</ListItemIcon>
							<ListItemText primary="Base User" />
						</ListItem>
					</List>

					<Divider />
					<Button
						onClick={ev => saveStressTest()}
						variant="contained"
						color="secondary"
						fullWidth
						style={{ marginTop: '50px' }}
					>
						Save
					</Button>

					<Button
						variant="contained"
						color="secondary"
						fullWidth
						style={{ marginTop: '20px' }}
						onClick={handleOpen}
					>
						Show stress test
					</Button>

					<Button
						variant="contained"
						color="secondary"
						fullWidth
						style={{ marginTop: '20px' }}
						onClick={runTest}
					>
						Run Test
					</Button>

					<Modal
						open={open}
						onClose={handleClose}
						aria-labelledby="Stress test json"
						aria-describedby="Stress test json"
					>
						{body}
					</Modal>
				</Grid>
				<Grid item xs={9} sm={9} lx={9} lg={9} md={9}>
					{viewType === VIEW_TYPE_MAIN && (
						<form className={classes.root} noValidate autoComplete="off">
							<div>
								<h2>Site</h2>
								<TextField
									variant="outlined"
									label="Domain name (e.g:  example.com)"
									fullWidth
									value={stressTest.header.domain}
									onChange={ev => setDomain(ev.target.value)}
								/>

								<Select
									value={stressTest.header.schema}
									onChange={ev => setSchema(ev.target.value)}
									displayEmpty
									className={classes.selectEmpty}
									inputProps={{ 'aria-label': 'Without label' }}
									style={{ width: '200px' }}
								>
									<MenuItem value="https">https</MenuItem>
									<MenuItem value="http">http</MenuItem>
								</Select>
							</div>
							<div>
								<h2>Hosts</h2>
								{stressTest.header.hosts.map((hostConfig, indexHost) => {
									return (
										<>
											<TextField
												variant="outlined"
												label="Host name for /etc/hosts"
												value={hostConfig.name}
												onChange={ev => setEtcHostName(ev.target.value, indexHost)}
											/>
											<TextField
												variant="outlined"
												label="Host name for /etc/hosts"
												value={hostConfig.ip}
												onChange={ev => setEtcHostIp(ev.target.value, indexHost)}
											/>
											<Button
												color="primary"
												variant="contained"
												onClick={ev => removeEtcHost(indexHost)}
											>
												Remove
											</Button>
										</>
									);
								})}
								<Button onClick={ev => addHostEntry()} variant="contained" color="secondary">
									Add Host for /etc/hosts
								</Button>
							</div>
						</form>
					)}
					{viewType === VIEW_TYPE_REQUESTS && selectedRequest && (
						<>
							<form className={classes.root} noValidate autoComplete="off">
								<div>
									<TextField
										variant="outlined"
										label="Request name"
										fullWidth
										value={stressTest.requests[selectedRequest].name}
										onChange={ev => onChangeRequestName(selectedRequest, ev.target.value)}
									/>
								</div>
								<div>
									<h3>Pre Requests</h3>
								</div>
								<div>
									<h3>Current Requests</h3>
									<TextField
										id="outlined-multiline-static"
										label="Curl Syntax"
										multiline
										rows={20}
										variant="outlined"
										onChange={ev => onChangeRequestCurl(selectedRequest, ev.target.value)}
										value={stressTest.requests[selectedRequest].curl}
									/>
									<TextField
										id="outlined-multiline-static"
										label="Bunnyshell Stress Testing Json"
										multiline
										rows={20}
										variant="outlined"
										value={JSON.stringify(stressTest.requests[selectedRequest].json_spec, null, 4)}
									/>
								</div>
								<div>
									<h3>Post Requests</h3>
									<div>
										{stressTest.requests[selectedRequest].post_requests.map(
											(postRequest, index) => {
												return (
													<div>
														<Select
															value={postRequest.request}
															onChange={ev =>
																changePostRequestName(ev.target.value, index)
															}
															displayEmpty
															className={classes.selectEmpty}
															inputProps={{ 'aria-label': 'Without label' }}
															style={{ width: '200px' }}
														>
															{Object.keys(stressTest.requests).map(requestName => {
																if (requestName !== selectedRequest) {
																	return (
																		<MenuItem value={requestName}>
																			{requestName}
																		</MenuItem>
																	);
																}
																return <></>;
															})}
														</Select>

														<TextField
															id="outlined-multiline-static"
															label="Bunnyshell Stress Testing Json"
															multiline
															rows={3}
															onChange={ev =>
																changePostRequestCode(ev.target.value, index)
															}
															variant="outlined"
															value={postRequest.pre_request_script}
														/>
													</div>
												);
											}
										)}
									</div>
									<Button
										onClick={ev => newPostRequest(selectedRequest)}
										variant="contained"
										color="secondary"
									>
										Add Post Request
									</Button>
								</div>
							</form>
						</>
					)}
					{viewType === VIEW_USER_BEHAVIOUR && (
						<form className={classes.root} noValidate autoComplete="off">
							{stressTest.users.user_basic[0].tasks.map((task, indexTask) => {
								return (
									<div>
										<TextField
											id="outlined-multiline-static"
											label="Percent"
											multiline
											rows={1}
											variant="outlined"
											onChange={ev => changeBehaviourRequestPercent(ev.target.value, indexTask)}
											value={task.percent}
										/>
										<FormControl className={classes.formControl}>
											<Select
												value={task.request}
												onChange={ev =>
													changeBehaviourRequestPercent(ev.target.value, indexTask)
												}
												displayEmpty
												className={classes.selectEmpty}
												inputProps={{ 'aria-label': 'Without label' }}
											>
												{Object.keys(stressTest.requests).map(requestName => (
													<MenuItem value={requestName}>{requestName}</MenuItem>
												))}
											</Select>
											<FormHelperText>Without label</FormHelperText>
										</FormControl>
									</div>
								);
							})}
						</form>
					)}

					{viewType === VIEW_RUN_TEST && (
						<div>
							<div style={{marginTop: '100px'}}>
								{runTestMessage.status === 'building_test' && <div>Building test...</div>}
								{runTestMessage.status === 'ok' && (
									<Link to="http://127.0.0.1:8089/" target="_blank" style={{margin: '100px'}}>
										Go to test platform
									</Link>
								)}
							</div>
							<div style={{marginTop: '100px'}}>
								<Typography variant="h6">Details</Typography>
								<div  >{JSON.stringify(runTestMessage)}</div>
							</div>
						</div>
					)}
				</Grid>
			</Grid>
		</>
	);
}

export default StressTesting;
