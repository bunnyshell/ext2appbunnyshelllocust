#!/usr/bin/env bash

source bin/common.sh

if [ -z "$1" ]; then
    echo "Docker compose file not found"
    exit 1
fi

docker_compose_file=$1
slave_number=$2

if ! test -f "$docker_compose_file"; then
    echo "$docker_compose_file does not exists."
    exit 2
fi

echo "Docker compose file is $docker_compose_file"

kill_remove_docker_containers
start_docker_containers