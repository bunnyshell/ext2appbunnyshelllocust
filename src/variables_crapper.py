import re
from cssselect import GenericTranslator, SelectorError
from lxml.html import fromstring
import requests
import logging
from src.template_functions  import auto_random_int_2, auto_random_int_4, build_webkit_form_boundaries
import random

def dom_css_first(css, attribute_name, document, get_text=False, var_type=None):
    try:
        expression = GenericTranslator().css_to_xpath(css)
        logging.info("Using css " + css + ", transformed in xpath: " + expression)
        # print("Using expression: %s" % expression)

        for e in document.xpath(expression):
            if not get_text:
                return e.get(attribute_name)
            else:
                if var_type == 'int' or var_type == "integer":
                    return int(e.text)

                return e.text
    except SelectorError:
        logging.info('Invalid selector. %s' % css)
        return None


def read_all_dom_attribute_with_css(css, attribute_name, document, var_type=None):
    try:
        expression = GenericTranslator().css_to_xpath(css)
        # print("Using expression: %s" % expression)
        values = []
        for e in document.xpath(expression):
            values.append(e.get(attribute_name))

        return values
    except SelectorError:
        print('Invalid selector. %s' % css)
        return []

def read_dom_count_elements(css, document, var_type=None):
    try:
        expression = GenericTranslator().css_to_xpath(css)
        # print("Using expression: %s" % expression)

        count = 0
        for e in document.xpath(expression):
            count = count + 1

        return count
    except SelectorError:
        print('Invalid selector. %s' % css)
        return -1


def read_dom_node_elements(css, document):
    try:
        expression = GenericTranslator().css_to_xpath(css)
        # print("Using expression: %s" % expression)

        elements = []
        for e in document.xpath(expression):
            elements.append(e)

        return elements
    except SelectorError:
        print('Invalid selector. %s' % css)
        return []


def extract_variables(extractors, response, extracted_variables):
    page_html = response.content.decode("UTF-8")
    document = fromstring(page_html)
    return extract_variables_from_html(extractors, document, page_html, extracted_variables)


def extract_variables_from_html(extractors, document, page_html, extracted_variables):
    for var_name, var_definition_query in extractors.items():
        var_definition = compile_extract_query(var_definition_query)
        var_extract_type = var_definition['extract']
        var_css = None
        var_attribute = None
        var_regex = None
        var_text = None

        if "css" in var_definition:
            var_css = var_definition['css']
            logging.info("extract using css")
        if "attribute" in var_definition:
            var_attribute = var_definition['attribute']
            logging.info("extract using attribute")
        if "text" in var_definition:
            var_text = var_definition['text']
            logging.info("extract using text")
        if "regex" in var_definition:
            var_regex = var_definition['regex']
            logging.info("extract using regex")

        if var_css is not None:
            if var_extract_type == "first_value":
                if var_attribute is not None:
                    logging.info("Select CSS first value....")
                    variable_value = dom_css_first(var_css, var_attribute, document)
                    extracted_variables[var_name] = variable_value
                elif var_regex is not None:
                    m = re.search(var_regex, page_html)
                    if m:
                        print(m.group(1))
                        variable_value = m.group(1)
                        extracted_variables[var_name] = variable_value
                elif var_text is not None:
                    variable_value = dom_css_first(var_css, var_attribute, document, get_text=True, var_type=var_definition['var_type'])
                    extracted_variables[var_name] = variable_value
                else:
                    raise ValueError("Please specify how to extract data: [ attribute | text ]")
            elif var_extract_type == "count_nodes":
                variable_value = read_dom_count_elements(var_css, document)
                extracted_variables[var_name] = variable_value
                # print("Computed extractor %s with value %s" % (var_name, variable_value))
            elif var_extract_type == "nodes":
                variable_value = read_dom_node_elements(var_css, document)
                extracted_variables[var_name] = variable_value
                # print("Computed extractor %s with value %s" % (var_name, variable_value))
            elif var_extract_type == "all_values":
                if var_attribute is not None:
                    values = read_all_dom_attribute_with_css(var_css, var_attribute, document, var_type=var_definition['var_type'])
                    extracted_variables[var_name] = values
                elif var_regex is not None:
                    m = re.search(var_regex, page_html)
                    if m:
                        print(m.group(1))
                        variable_value = m.group(1)
                        extracted_variables[var_name] = variable_value
                elif var_text is not None:
                    raise ValueError("NOT IMPLEMENTED #324")
                else:
                    raise ValueError("Please specify how to extract data: [ attribute | text ]")

    return extracted_variables

compiled_evals = {}

def identify_external_variables(string, external_variables_name="external_variables"):
    if "{{auto_random_int_2}}" in string:
        string = string.replace("{{auto_random_int_2}}", '" + str(auto_random_int_2()) + "')

    if "{{auto_random_int_4}}" in string:
        string = string.replace("{{auto_random_int_4}}", '" + str(auto_random_int_4()) + "')

    is_open_mark = False
    prev_char = None
    buffer = ""
    copy_string = string
    for char in string:
        if not is_open_mark and char == "{" and prev_char == "{":
            is_open_mark = True
            continue
        elif is_open_mark and char == "}" and prev_char =="}":
            buffer_eval = buffer.strip()
            print("detected variable `%s`" % buffer_eval)
            is_open_mark = False
            prev_char = None
            if buffer_eval.startswith("eval:"):
                query = buffer_eval[len("eval:"):].strip()
                if len(query) == 0:
                    raise ValueError("Eval expression can not be empty")
                print("is eval expression `%s`" % query)
                if query not in compiled_evals:
                    compiled_evals[query] = compile(query, "<string>", "eval")
                copy_string = copy_string.replace("{{" + buffer + "}}", str(eval(compiled_evals[query])))
            elif "random.choice" in buffer:
                copy_string = copy_string.replace("{{" + buffer + "}}", "\" + " + buffer.strip() + " + \"")
            else:
                copy_string = copy_string.replace("{{" + buffer + "}}", "\" + external_variables['" + buffer.strip() + "'] + \"")
            buffer = ""

            continue
        elif is_open_mark and char != "}":
            if is_open_mark:
                buffer = buffer + char
        prev_char = char

    return copy_string

#@deprecated @see context service
def in_place_identify_external_variables(string, external_variables):
    if "{{auto_random_int_2}}" in string:
        string = string.replace("{{auto_random_int_2}}", str(auto_random_int_2()))

    if "{{auto_random_int_4}}" in string:
        string = string.replace("{{auto_random_int_4}}", str(auto_random_int_4()))

    is_open_mark = False
    prev_char = None
    buffer = ""
    copy_string = string
    var_type = str

    for char in string:
        if not is_open_mark and char == "{" and prev_char == "{":
            is_open_mark = True
            continue
        elif is_open_mark and char == "}" and prev_char =="}":
            buffer_eval = buffer.strip()
            print("detected variable `%s`" % buffer_eval)
            is_open_mark = False
            prev_char = None

            if buffer_eval.startswith("eval:"):
                query = buffer_eval[len("eval:"):].strip()
                if len(query) == 0:
                    raise ValueError("Eval expression can not be empty")
                print("is eval expression `%s`" % query)
                if query not in compiled_evals:
                    compiled_evals[query] = compile(query, "<string>", "eval")
                copy_string = copy_string.replace("{{" + buffer + "}}", str(eval(compiled_evals[query])))
            else:
                buffer_key = buffer.strip()
                var_type = str
                if ":" in buffer_key:
                    buffer_key_parts = buffer_key.split(":")
                    buffer_key = buffer_key_parts[0].strip()
                    var_type = buffer_key_parts[1].strip()
                    if var_type == 'int':
                        var_type = int

                # if we have to replace a single parameter and is specified to of type int, make it int at the end
                if string == "{{" + buffer + "}}":
                    if isinstance(external_variables[buffer_key], list):
                        return var_type(copy_string.replace("{{" + buffer + "}}", str(random.choice(external_variables[buffer_key]))))
                    else:
                        return var_type(copy_string.replace("{{" + buffer + "}}", str(external_variables[buffer_key])))

                # orginal string has more chars then this parameters, assume is string
                if isinstance(external_variables[buffer_key], list):
                    copy_string = copy_string.replace("{{" + buffer + "}}", str(random.choice(external_variables[buffer_key])))
                else:
                    copy_string = copy_string.replace("{{" + buffer + "}}", str(external_variables[buffer_key]))

            buffer = ""

            continue
        elif is_open_mark and char != "}":
            if is_open_mark:
                buffer = buffer + char
        prev_char = char

    return copy_string


compiled_queries = {}

def compile_extract_query(query):
    if query in compiled_queries:
        return compiled_queries[query]

    parts = query.split("=>")
    if len(parts) < 2:
        raise ValueError("Query must have three parts: <selection mode> => <selection query> => <value query>, found: " + str(len(parts)))
    selection_mode = parts[0].strip()
    selection_query = parts[1].strip()
    value_query = None
    var_type = None
    if len(parts) >= 3:
        value_query = parts[2].strip()
    if len(parts) >= 4:
        var_type = parts[3].strip()

    valid_selection_modes = ["first_value", "count_nodes", "nodes", "all_values"]
    if selection_mode not in valid_selection_modes:
        raise ValueError("Selection mode not recognized")

    if selection_mode in ["first_value", "all_values"] and len(parts) < 3:
        raise ValueError("Query must have three parts: <selection mode> => <selection query> => <value query>, found: " + str(len(parts)))

    selection_query_parts = selection_query.split(":")
    if len(selection_query_parts) != 2:
        raise ValueError("#2 Selection query must have two parts; <selection type>:<selection query select>")

    selection_query_type = selection_query_parts[0].strip()
    selection_query_select = selection_query_parts[1].strip()

    valid_selection_types= ['css']
    if not selection_query_type in valid_selection_types:
        raise ValueError("Selection type invalid")


    query_data = None
    if value_query is not None:
        value_query_parts = value_query.split(":")

        if len(value_query_parts) < 1:
            raise ValueError("#1 Selection query must have two parts; <selection type>:<selection query select>")

        value_query_type = value_query_parts[0].strip()

        valid_value_query_types = ['attribute', "text"]

        if not value_query_type in valid_value_query_types:
            raise ValueError("Selection type invalid")

        if value_query_type == "attribute":
            if len(value_query_parts) != 2:
                raise ValueError("You must specify the attribute name for %s" % query)
            value_query_select = value_query_parts[1].strip()

            query_data = {
                "extract": selection_mode,
                selection_query_type: selection_query_select,
                value_query_type: value_query_select,
                'var_type': var_type
            }
        elif value_query_type == "text":
            query_data = {
                "extract": selection_mode,
                selection_query_type: selection_query_select,
                value_query_type: "_entire_text_",
                'var_type': var_type
            }
        # elif value_query_type == "text" and len(value_query_parts) == 2:
        #     # todo get text regex and replace _entire_text_
        #     query_data = {
        #         "extract": selection_mode,
        #         selection_query_type: selection_query_select,
        #         value_query_type: "_entire_text_"
        #     }
        else:
            raise ValueError("Not implemented")
    else:
        query_data = {
            "extract": selection_mode,
            selection_query_type: selection_query_select,
            'var_type': var_type
        }

    compiled_queries[query] = query_data

    return query_data
