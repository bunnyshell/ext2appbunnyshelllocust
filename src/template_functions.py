import random, string

#@deprecated @see context service
def auto_random_int_2():
    return random.randint(10, 99)

#@deprecated @see context service
def auto_random_int_4():
    return random.randint(1000, 9999)


def auto_random_int_16():
    return str(auto_random_int_4()) + str(auto_random_int_4()) + str(auto_random_int_4()) + str(auto_random_int_4())

def build_webkit_form_boundaries(payload):
    webkit_from_boundaries = ""
    webkit_str = ''.join(random.choices(string.ascii_letters + string.digits, k=16))
    webkit = "------WebKitFormBoundary" + webkit_str
    for key, value in payload.items():
        webkit_from_boundaries = webkit_from_boundaries + webkit + '\r\nContent-Disposition: form-data; name="' + str(key) + '"\r\n\r\n' + str(value) + '\r\n'
    webkit_from_boundaries = webkit_from_boundaries + webkit + '--\r\n'

    return webkit_from_boundaries

#@deprecated @see context service
def random_int_min_max(min, max):
    random.randint(min, max)