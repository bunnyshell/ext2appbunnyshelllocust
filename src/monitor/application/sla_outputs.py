# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime, read_config
from src.monitor.application.sla.sla_request import sla_request
from src.monitor.application.sla.sla_mysql import sla_mysql
from src.monitor.application.sla.sla_elasticsearch_query import sla_elasticsearch_query
from src.monitor.application.sla.sla_ssl_expiration import sla_ssl_expiration
from threading import Lock
from jinja2 import Template
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.message import EmailMessage

class SLAFileOutput:
    """
    Output handler for SLA tests
    """
    file_path = None

    def __init__(self, file_path):
        if not os.path.isfile(file_path):
            f = open(file_path, "w")
            f.write("SLA Results")
            f.close()

        self.file_path = file_path


    def write_result(self, result, test_name, run_identity):
        """
        Write test results to a file

        :param result:
        :param test_name:
        :param run_identity:
        :return:
        """
        file_object = open(self.file_path, 'a')
        file_object.write(str(run_identity) + ' - Test ' + test_name + ": " + str(result) + "\n")
        file_object.close()


class SLAInfluxOutput:
    """
    Output handler for SLA tests
    """
    influxClient = None

    measurement_prefix = None

    def __init__(self, influxClient, measurement_prefix):
        self.influxClient = influxClient
        self.measurement_prefix = measurement_prefix

    def write_result(self, result, test_name, run_identity):
        """
        Write test results to a influx

        :param result:
        :param test_name:
        :param run_identity:
        :return:
        """
        if self.measurement_prefix is None:
            measurement_name = "sla." + test_name
        else:
            measurement_name = self.measurement_prefix + "." + test_name
        logging.info("Influx writing measurement %s with result value %s" % (measurement_name, str(result)))
        if result is None or not isinstance(result, int):
            result = 2

        data = [{
            "measurement": measurement_name,
            "tags": {
                "test_name": test_name,
                "result": result,
                "identity": run_identity
            },
            "time": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
            "fields": {
                "value": result
            },
        }]
        print("Write influx data: " + str(data))
        try:
            self.influxClient.write_points(data, time_precision='ms')
        except Exception as e:
            logging.error("Could not save data to InfluxDB: %s" % e)
            print("Influx ERR: " + str(e))


class SLAEmailOutput:

    smtp_host = None
    smtp_port = None
    smtp_user = None
    smtp_password = None
    email_from = None
    email_to = None

    def __init__(self, smtp_host, smtp_port, smtp_user, smtp_password, email_from, email_to):
        self.smtp_host = smtp_host
        self.smtp_port = smtp_port
        self.smtp_user = smtp_user
        self.smtp_password = smtp_password
        self.email_from = email_from
        self.email_to = email_to

    def write_general_report(self, sla_results, sla_suite_summary):
        f = open("templates/email/sla_email.html", "r")
        file_content = f.read()
        template = Template(file_content)

        html_content = template.render(
            component_name="TestComponent",
            status="Alerting",
            sla_results=sla_results,
            sla_suite_summary=sla_suite_summary
        )
        self.send_email(html_content)

    def send_email(self, html_content):
        message = MIMEMultipart("alternative")
        message["Subject"] = "Bunnyshell - System Checking Report"
        message["From"] = self.email_from
        message["To"] = self.email_to

        part1 = MIMEText(html_content, "plain")
        part2 = MIMEText(html_content, "html")

        message.attach(part1)
        message.attach(part2)

        context = ssl.SSLContext(ssl.PROTOCOL_TLS)
        with smtplib.SMTP(self.smtp_host, int(self.smtp_port)) as server:
            server.ehlo()  # Can be omitted
            server.starttls(context=context)
            server.ehlo()  # Can be omitted
            # server.login(sender_email, password)
            server.login(self.smtp_user, self.smtp_password)
            server.sendmail(
                self.email_from, self.email_to, message.as_string()
            )


def main():

    sla_results = [
        {
            "sla_meta": {
                "sla_name": "Swap",
                "vm_name": "ProdInfra",
                "vm_public_ip": "127.0.0.1",
                "vm_private_ip": "127.0.0.1",
            },
            "sla_result": "OK",
            "sla_logs": "here logs"
        },
        {
            "sla_meta": {
                "sla_name": "Swap",
                "vm_name": "ProdInfra",
                "vm_public_ip": "127.0.0.1",
                "vm_private_ip": "127.0.0.1",
            },
            "sla_result": "OK",
            "sla_logs": "here logs"
        }
    ]

    out = SLAEmailOutput("smtp.mailtrap.io", 2525, "19486daa9cd5d897d", "782cd12d09ff87", "norely@bunnyshell.com", "ionut.tocila.x@gmail.com")
    out.write_general_report(sla_results, "xx", "norely@bunnyshell.com", "ionut.tocila.x@gmail.com")


def print_ssh_result_logs(result_logs):
    logs = result_logs.split('\\n')
    for log in logs:
        logging.info("    " + log)


if __name__ == '__main__':
    main()