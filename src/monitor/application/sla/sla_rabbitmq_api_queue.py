# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
import datetime
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime, make_request


def sla_rabbitmq_api_queue(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["type", "url", "verify", "timeout", "headers", "assertions"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    response = make_request(action_specs, external_variables)
    logging.info(test_context + " RabbittMQ response code: " + str(response.status_code))

    response_result = response.json()
    logging.info(test_context + " " + str(response_result))

    total_queues = len(response_result)
    total_consumers = 0

    for data in response_result:
        total_consumers += data['consumers']

    logging.info(test_context + " Total consumers: " + str(total_consumers) + ", total queues: " + str(total_queues))

    # fail = False
    # for queue_info in response_result:
    #     if 'delayed' in queue_info['name']:
    #         continue
    #
    #     if queue_info['consumers'] == 0:
    #         logging.info(test_context + " [%s] Queue does not have any consumer" % (queue_info['name']))
    #         fail = True
    #
    # if fail is True:
    #     return 1

    return 0, ""