# coding=utf-8
import logging
import mysql.connector
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
from jsonschema import validate
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime
from threading import Lock
import argparse
import sys
import base64
import json

mutex_mysql = Lock()


def sla_mysql(test_context, action_specs, external_variables, dbal_connections):
    """
    Performs mysql operations

    :param dbal_connections:
    :param test_context:
    :param action_specs:
    :param action_type:
    :param external_variables:
    :return:
    """
    print("Connect to database")
    action_config_mandatory_keys = ["dbal_connection", "select_single_scalar"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)
    dbal_connection_name = action_specs["dbal_connection"]
    dbal_connection_params = dbal_connections[dbal_connection_name]

    sql_query = None
    if "select_single_scalar" in action_specs.keys():
        sql_query = in_place_identify_external_variables(action_specs["select_single_scalar"], external_variables)
    if sql_query is None:
        raise ValueError("Mysql Query is not defined")

    print(test_context + " Executing SQL query: " + sql_query)

    result = execute_query(dbal_connection_params, sql_query, lambda l_cursor: l_cursor.fetchone()[0])
    if action_specs['type'] == "int" or action_specs['type'] == "integer":
        result = int(result)

    print(test_context + " Query result:" + str(result))

    if "save_in_variable" in action_specs.keys():
        external_variables[action_specs['save_in_variable']] = result

    has_assertions = False
    if "assertions" in action_specs.keys():
        logging.info(test_context + " Asserting...")
        if "variables_json_schema" in action_specs['assertions'].keys():
            validate(instance=external_variables, schema=action_specs['assertions']['variables_json_schema'])
            logging.info(test_context + " Assertions variables_json_schema validated")
            has_assertions = True
    else:
        raise ValueError("Can not select")

    if not has_assertions:
        raise ValueError("Each test must have assertions in order to validate that test was OK")

    return 0, ""


def execute_query(dbal_connection_params, sql_query, fetch_fcn):
    """
    Opens new connection and execute query
    Since mysql connector is not thread safe we must protect this

    :param dbal_connection_params:
    :param sql_query:
    :param fetch_fcn:
    :return:
    """
    mutex_mysql.acquire()
    try:
        print("create connection: " + str(dbal_connection_params))
        connection = mysql.connector.connect(
            host=dbal_connection_params['mysql_host'],
            database=dbal_connection_params['mysql_database'],
            user=dbal_connection_params['mysql_user'],
            password=dbal_connection_params['mysql_password'],
            connect_timeout=3
        )

        try:
            print("execute query in cursor")
            cursor = connection.cursor()
            cursor.execute(sql_query)
            result = fetch_fcn(cursor)
            cursor.close()

            print("Mysql query result is: " + str(result))

            return result
        finally:
            print("close connection")
            if connection.is_connected():
                connection.close()
    finally:
        mutex_mysql.release()

    return None


if __name__ == "__main__":
    print("Execute main thread")
    parser = argparse.ArgumentParser(description="Mysql sla test executor")
    parser.add_argument('--test-env', nargs='?', help='Test context', required=True)
    parse_args = sys.argv[1:]
    arguments = vars(parser.parse_args(args=parse_args))

    if arguments["test_env"] is None:
        raise ValueError("Test env can not be none")

    test_env = arguments['test_env']
    test_env = json.loads(base64.b64decode(test_env.encode('ascii')).decode('ascii'))

    try:
        print("Execute mysql sla")
        result, logs = sla_mysql(
            test_env['context'],
            test_env['action_specs'],
            test_env['external_variables'],
            test_env['dbal_connections'],
        )
        if result == 0:
            print("[sla_ok] mysql test passed")
        else:
            print("[sla_not_ok] mysql test NOT passed")
    except Exception as e:
        print("[sla_not_ok] exception: " + str(e))
