# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
import datetime
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime

def sla_elasticsearch_query(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["query_url", "query"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)
    elasticsearch_query = action_specs['query']

    elasticsearch_query = replace_object_parameters(elasticsearch_query, external_variables)

    logging.info(test_context + " Executing Elasticsearch query: " + str(elasticsearch_query))

    response = requests.post(
        action_specs['query_url'],
        data=json.dumps(elasticsearch_query),
        headers={
            "Content-type": "application/json",
            "Accept": "application/json"
        }
    )

    if response.status_code != 200:
        logging.error(test_context + " [%s] Status code when getting elasticsearch query was %s with message: %s" %
                      (test_name, response.status_code, response.content))
        return None, ""

    query_result = response.json()

    logging.info(test_context + " " + str(query_result))

    has_assertions = False
    if "assertions" in action_specs.keys():
        if "json_schema" in action_specs['assertions'].keys():
            logging.info(test_context + " Asserting...")
            validate(instance=query_result, schema=action_specs['assertions']['json_schema'])
            logging.info(test_context + " Assertions json_scema validated")
            has_assertions = True

    if not has_assertions:
        raise ValueError("Each test must have assertions in order to validate that test was OK")

    return 0, ""