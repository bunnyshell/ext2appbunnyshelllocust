#/usr/bin/env bash
set -e

output=`service bunnyshell status | grep 'Active:' | grep 'active (running)' | wc -l`
if [ $output -eq 1 ]; then
  echo "[sla_ok] Bunnyshell agent running ok"
else
  echo "[sla_failed] Bunnyshell agent not running"
fi