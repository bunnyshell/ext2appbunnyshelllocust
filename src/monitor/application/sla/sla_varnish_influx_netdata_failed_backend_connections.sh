#/usr/bin/env bash
set -e

echo "influx database is $1"
echo "Influx metric is $2"
echo "Up threshbold is $3"

current_value=`influx -execute "SELECT MEAN(\"value\") FROM \"$2\" WHERE (\"host\" =~ /^varnish-prod7e16b2_10\.110\.0\.18_864a6fedf73fc1ba17d4436288776d92.*$/) AND time >= now() - 2m fill(0)" -database $1 | tail -n 1 | cut -d" " -f2 |  xargs printf`
echo "Mean value for varnish -> netdata influx metrics -> netdata.varnish.current_poll_hit_rate.hit is $current_value"

if [ $current_value -gt $3 ];then
  echo "[sla_failed] Varnish check on metric $2 has value $current_value, grater then $3";
  exit 1;
fi

echo "[sla_ok] Varnish metric $2 from database $1 is in state ok with value $current_value"