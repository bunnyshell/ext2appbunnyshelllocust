#/usr/bin/env bash
set -e

# https://serverfault.com/questions/189320/how-can-i-monitor-what-logrotate-is-doing#:~:text=To%20verify%20if%20a%20particular,which%20it%20was%20last%20rotated.
# if logrotate is triggered by another user then status is fount ad $HOME/logrotate/logrotate-state
root_log_rotate_count=`cat /var/lib/logrotate/status | grep -v "logrotate state " | wc -l | xargs printf`
echo "Sever log rotation count $root_log_rotate_count"

if [ $root_log_rotate_count -lt 10 ]; then
  echo "[sla_failed] Server log rotation status has $root_log_rotate_count entries, expected 10";
  exit 1;
fi

echo "[sla_ok] Sever log rotation enabled"

