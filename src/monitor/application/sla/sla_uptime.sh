#/usr/bin/env bash
set -e

uptime_days=`uptime | cut -d "," -f 1 | tr -s " " | cut -d " " -f 4 | xargs printf`
echo "Sever is up for $uptime_days days"

if [ $uptime_days -lt 30 ]; then
  echo "[sla_failed] Server was restarted more recenlty then 30 days";
  exit 1;
fi

echo "[sla_ok] Server uptime is ok, up and running more then 30 days"
