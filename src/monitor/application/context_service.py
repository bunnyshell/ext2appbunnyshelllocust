import re
from cssselect import GenericTranslator, SelectorError
from lxml.html import fromstring
import requests
import logging
import random

compiled_evals = {}


class ContextService:

    @staticmethod
    def replace_object_parameters(object, external_variables):
        """

        Replace in dict the values set as parameters by going in depth

        :param object:
        :param external_variables:
        :return:
        """
        if not isinstance(object, dict) and not isinstance(object, list):
            return object

        if isinstance(object, dict):
            new_object = {}
            for key, value in object.items():
                if isinstance(value, dict):
                    new_object[key] = ContextService.replace_object_parameters(value, external_variables)
                elif isinstance(value, list):
                    for k in value:
                        new_object[key] = ContextService.replace_object_parameters(value, external_variables)
                elif isinstance(value, str):
                    new_object[key] = ContextService.in_place_identify_external_variables(value, external_variables)
                else:
                    new_object[key] = value

            return new_object

        if isinstance(object, list):
            new_list = []
            for value in object:
                new_list.append(ContextService.replace_object_parameters(value, external_variables))

            return new_list

    @staticmethod
    def in_place_identify_external_variables(string, external_variables):
        if "{{auto_random_int_2}}" in string:
            string = string.replace("{{auto_random_int_2}}", str(ContextService.auto_random_int_2()))

        if "{{auto_random_int_4}}" in string:
            string = string.replace("{{auto_random_int_4}}", str(ContextService.auto_random_int_4()))

        is_open_mark = False
        prev_char = None
        buffer = ""
        copy_string = string
        var_type = str

        for char in string:
            if not is_open_mark and char == "{" and prev_char == "{":
                is_open_mark = True
                continue
            elif is_open_mark and char == "}" and prev_char =="}":
                buffer_eval = buffer.strip()
                print("detected variable `%s`" % buffer_eval)
                is_open_mark = False
                prev_char = None

                if buffer_eval.startswith("eval:"):
                    query = buffer_eval[len("eval:"):].strip()
                    if len(query) == 0:
                        raise ValueError("Eval expression can not be empty")
                    print("is eval expression `%s`" % query)
                    if query not in compiled_evals:
                        compiled_evals[query] = compile(query, "<string>", "eval")
                    copy_string = copy_string.replace("{{" + buffer + "}}", str(eval(compiled_evals[query])))
                else:
                    buffer_key = buffer.strip()
                    var_type = str
                    if ":" in buffer_key:
                        buffer_key_parts = buffer_key.split(":")
                        buffer_key = buffer_key_parts[0].strip()
                        var_type = buffer_key_parts[1].strip()
                        if var_type == 'int':
                            var_type = int

                    # if we have to replace a single parameter and is specified to of type int, make it int at the end
                    if string == "{{" + buffer + "}}":
                        if isinstance(external_variables[buffer_key], list):
                            return var_type(copy_string.replace("{{" + buffer + "}}", str(random.choice(external_variables[buffer_key]))))
                        else:
                            return var_type(copy_string.replace("{{" + buffer + "}}", str(external_variables[buffer_key])))

                    # orginal string has more chars then this parameters, assume is string
                    if isinstance(external_variables[buffer_key], list):
                        copy_string = copy_string.replace("{{" + buffer + "}}", str(random.choice(external_variables[buffer_key])))
                    else:
                        copy_string = copy_string.replace("{{" + buffer + "}}", str(external_variables[buffer_key]))

                buffer = ""

                continue
            elif is_open_mark and char != "}":
                if is_open_mark:
                    buffer = buffer + char
            prev_char = char

        return copy_string

    @staticmethod
    def auto_random_int_2():
        return random.randint(10, 99)

    @staticmethod
    def auto_random_int_4():
        return random.randint(1000, 9999)

    @staticmethod
    def random_int_min_max(min, max):
        random.randint(min, max)