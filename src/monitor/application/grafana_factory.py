import yaml


factory_config = {
    "template_monitoring_panel": 'src/monitor/domain/template/grafana_monitoring_panel.json'
}


class GrafanaFactory:

    @staticmethod
    def factory_monitoring_panel(panel_id, alert_name, data_source, measurement):
        """
        Factory monitoring panel

        :return:
        """
        with open(factory_config["template_monitoring_panel"], 'r') as stream:
            try:
                template = yaml.safe_load(stream)
                template['alert']['name'] = alert_name
                template['title'] = alert_name
                template['id'] = panel_id
                template['datasource'] = data_source
                template['targets'][0]['measurement'] = measurement

                return template
            except yaml.YAMLError as exc:
                print(exc)
                exit(1)