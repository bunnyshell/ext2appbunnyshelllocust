import requests
import json


class GrafanaService:

    grafana_api_key = None

    grafana_url = None

    def __init__(self, grafana_api_key, grafana_url):
        self.grafana_api_key = grafana_api_key
        self.grafana_url = grafana_url

    def get_dashboard_by_uid(self, uid):
        """
        Get grafana dashboard by uid
        :param id:
        :return:
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + self.grafana_api_key
        }

        response = requests.get(
            url=self.grafana_url + "/api/dashboards/uid/" + uid,
            headers=headers,
            data={}
        )
        if response.status_code != 200:
            raise ValueError("The dashboard response code is not 200")

        decoded_content = response.content.decode('utf-8', errors="replace")
        return json.loads(decoded_content)

    def update_dashboard_json(self, dashboard_update_payload):
        """
        Updates the grafana dashboard

        :param dashboard_update_payload:
        :return:
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + self.grafana_api_key
        }

        payload = json.dumps(dashboard_update_payload)
        response = requests.post(
            url=self.grafana_url + "/api/dashboards/db",
            headers=headers,
            data=payload
        )
        print("Response code: " + str(response.status_code))
        print("Response content: " + str(response.content.decode("utf-8")))
        if response.status_code != 200:
            raise ValueError("The dashboard response code is not 200")