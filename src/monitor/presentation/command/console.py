from __future__ import annotations
from abc import ABC, abstractmethod
import argparse
from src.get_request import GetRequest
from src.monitor.presentation.command.FactoryBasicMonitoringCommand import FactoryBasicMonitoringCommand
from src.monitor.presentation.command.BuildMonitoringCommand import BuildMonitoringCommand
from src.monitor.presentation.command.ExecuteSlaDefinitionCommand import ExecuteSlaDefinitionCommand
from src.monitor.application.file_service import FileService
import logging
import sys
import os

class Application:
    commands = {}

    def __init__(self):
        pass

    def register(self, command):
        self.commands[command.name] = command
        print("Register command %s as %s" % (command.name, str(command) ))

    def execute(self, command_name, *kargs):
        cmd = None
        for name, command in self.commands.items():
            if name == command_name:
                cmd = command
                break

        if cmd is None:
            raise ValueError("Command not found")

        print("execute command %s " % command_name)
        cmd.execute()


if __name__ == "__main__":
    """
    The client code can parameterize an invoker with any commands.
    """
    param_file = "config/parameters.yml"
    if not os.path.isfile(param_file):
        raise ValueError("Parameter file not found, specify " + param_file)

    app_parameters = FileService.read_yml_or_dict(param_file)

    application = Application()
    application.register(FactoryBasicMonitoringCommand(app_parameters))
    application.register(BuildMonitoringCommand(app_parameters))
    application.register(ExecuteSlaDefinitionCommand(app_parameters))

    print("----RUN CMD---")
    application.execute(sys.argv[1])