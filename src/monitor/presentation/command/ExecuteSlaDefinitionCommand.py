from Command import Command
import argparse
from src.monitor.application.grafana_factory import GrafanaFactory
from src.monitor.application.grafana_service import GrafanaService
from src.monitor.application.context_service import ContextService
from src.monitor.application.file_service import FileService
import requests
import json
import sys
import os
import logging
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
import datetime
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime, read_config
from src.monitor.application.sla.sla_request import sla_request
from src.monitor.application.sla.sla_mysql import sla_mysql
from src.monitor.application.sla.sla_remote_ssh import sla_bin_remote_ssh
from src.monitor.application.sla.sla_elasticsearch_query import sla_elasticsearch_query
from src.monitor.application.sla.sla_ssl_expiration import sla_ssl_expiration
from threading import Lock
from src.monitor.application.sla_outputs import SLAFileOutput, SLAInfluxOutput, SLAEmailOutput
from src.monitor.application.sla.sla_rabbitmq_api_queue import sla_rabbitmq_api_queue
import traceback
import uuid
from src.monitor.application.sla.ssh_sla import sla_varnish_influx_netdata_failed_backend_connections, ssh_exec_file_remotly, sla_ssh_remote_swap, sla_ssh_remote_bunnyshell_agent, sla_ssh_remote_digital_ocean_agent, sla_ssh_uptime_more_then_x_days, sla_ssh_log_rotation_enabled
from Crypto.PublicKey import RSA
import rsa
import base64
from src.monitor.application.sla_outputs import print_ssh_result_logs

mutex_output = Lock()
stop_program = False

class ExecuteSlaDefinitionCommand(Command):

    name = "app:monitoring:execute:definition"
    description = """
    
    Factory grafana charts, builds a single config file which contains all settings for a single definition
     eg: 
     
python src/monitor/presentation/command/console.py app:monitoring:execute:definition  --definition /Users/ingres/work/bunnyshell/x1_project/sla_specs/build/build_bestvalue.yml --params /Users/ingres/work/bunnyshell/x1_project/sla_specs/definition/bestvalue/parameters.yml

    """

    def __init__(self, parameters) -> None:
        """
        Complex commands can accept one or several receiver objects along with
        any context data via the constructor.
        """
        self.parameters = parameters
        self.parser = argparse.ArgumentParser(description=self.description)
        self.parser.add_argument('--definition', nargs='?', help='Path to definition config file', required=True)
        self.parser.add_argument('--params', nargs='?', help='Path to parameters', required=True)
        self.parser.add_argument('--only', nargs='?', help='Specify a test name to be executed only')

    def execute(self) -> None:
        parse_args = sys.argv[2:]
        arguments = vars(self.parser.parse_args(args=parse_args))

        definition = arguments['definition']
        file_params = arguments['params']
        only_definition = arguments['only']

        print("Using ONLY definition: " + str(only_definition))

        if not os.path.isfile(definition):
            raise ValueError(str(definition) + " is not a definition file")

        if not os.path.isfile(file_params):
            raise ValueError(str(file_params) + " is not a parameter file")

        test_definition = FileService.read_yml_or_dict(definition)
        parameters = FileService.read_yml_or_dict(file_params)
        test_definition['parameters'] = parameters

        if only_definition is not None:
            if only_definition not in test_definition['sla_tests'].keys():
                raise ValueError("The only option " + only_definition + " does not exists as sla test")
            test_definition['sla_tests'] = {
                only_definition: test_definition['sla_tests'][only_definition]
            }
            #cancel any asnc mode and set sync
            test_definition['run_mode'] = "not_async"

        if "dbal_conections" in test_definition.keys():
            test_definition['dbal_conections'] = ContextService.replace_object_parameters(
                test_definition['dbal_conections'],
                test_definition['parameters']
            )
        else:
            test_definition['dbal_conections'] = {}

        if "outputs" in test_definition:
            test_definition['outputs'] = ContextService.replace_object_parameters(
                test_definition['outputs'],
                test_definition['parameters']
            )
        else:
            test_definition['outputs'] = {}

        config_logging(test_definition['logging_file_path'])
        output_clients = self.factory_outputs(test_definition)
        start_tests(test_definition, output_clients)

    def factory_outputs(self, sla_test_map):
        """
        Factory all tests outputs like influx/files
        :param sla_test_map:
        :return:
        """
        output_clients = {}
        for output_name, output_config in sla_test_map['outputs'].items():
            logging.info("[process output client] Processing output " + output_name)
            if output_config['type'] == "influx":
                logging.info("[process output client] type is influx")
                mandatory_keys = ["influx_host", "influx_port", "influx_database", "measurement_prefix", "influx_user", "influx_password"]
                verify_mandatory_key_exists(mandatory_keys, output_config)
                client = InfluxDBClient(
                    host=output_config['influx_host'],
                    port=output_config['influx_port'],
                    username=output_config['influx_user'],
                    password=output_config['influx_password'],
                    verify_ssl=False,
                    timeout=5
                )
                client.switch_database(output_config['influx_database'])

                output_clients[output_name] = SLAInfluxOutput(client,output_config['measurement_prefix'])
                logging.info("[process output client]  influxb client created")
            elif output_config['type'] == "file":
                mandatory_keys = ["path"]
                verify_mandatory_key_exists(mandatory_keys, output_config)
                output_clients[output_name] = SLAFileOutput(output_config['path'])
            elif output_config['type'] == "email":
                mandatory_keys = ["smtp_host", "smtp_port", "smtp_user", "smtp_password", "from", "to"]
                verify_mandatory_key_exists(mandatory_keys, output_config)
                output_clients[output_name] = SLAEmailOutput(
                    output_config['smtp_host'],
                    output_config['smtp_port'],
                    output_config['smtp_user'],
                    output_config['smtp_password'],
                    output_config['from'],
                    output_config['to']
                )
            else:
                raise ValueError("Output type not found")

        return output_clients


def start_tests(sla_test_map, output_clients):
    """
    Starts tests in async mode or sync

    :param sla_test_map:
    :param output_clients:
    :return:
    """
    threads = []
    run_mode = sla_test_map['run_mode']
    interval = 0
    if run_mode == "async_interval":
        interval = int(sla_test_map['interval'])
    if run_mode == "async_interval" and interval <= 0:
        raise ValueError("Run mode async_interval requires that interval values should be grater then zero")

    sla_results = []
    index = 1
    for test_name, test_config in sla_test_map['sla_tests'].items():
        if run_mode == "async_interval":
            test_thread = start_test_thread(test_name, test_config, output_clients, sla_test_map)
            threads.append(test_thread)
        else:
            index = index + 1
            result, logs = run_sync_test(test_name, test_config, output_clients, sla_test_map)
            logging.info("[Done] sync test `" + test_name + "`")
            test_data = {}
            test_data.update(test_config['action'])
            test_data['sla_result'] = result
            test_data['sla_logs'] = logs
            test_data['index'] = index
            if 'suite' in test_config.keys():
                test_data['suite'] = test_config['suite']
            else:
                test_data['suite'] = "general"

            sla_results.append(test_data)

    if len(threads) > 0:
        print("Run in async mode...")
        signal.pause()
        for thread in threads:
            thread.join()
    else:
        logging.info("Run in sync mode, sending result...")
        # logging.info(str(sla_results))

        sla_suite_summary = {}
        for sla_result in sla_results:
            print_ssh_result_logs(str(sla_result["sla_logs"]))
            suite = sla_result['suite']
            if suite not in sla_suite_summary.keys():
                sla_suite_summary[suite] = {
                    'success': 0,
                    'failed': 0,
                    'failed_details': []
                }

            if sla_result['sla_result'] == 0:
                sla_suite_summary[suite]['success'] = sla_suite_summary[suite]['success'] + 1
            else:
                sla_suite_summary[suite]['failed'] = sla_suite_summary[suite]['failed'] + 1
                if "sla_meta" in sla_result.keys():
                    sla_suite_summary[suite]['failed_details'].append(sla_result['sla_meta'])

        if "general_report_outputs" in sla_test_map.keys():
            for general_report_output in sla_test_map['general_report_outputs']:
                logging.info("Sending general reports on output " + general_report_output)
                client = output_clients[general_report_output]
                client.write_general_report(sla_results=sla_results, sla_suite_summary=sla_suite_summary)


def start_test_thread(test_name, test_config, output_clients, sla_test_map):
    """
    Starts  a test thread, used to perform SLA tests on a specific interval

    :param test_name:
    :param influxdb_client:
    :param run_function:
    :param data:
    :return:
    """
    try:
        logging.info("Starting async thread test %s" % test_name)

        test_thread = Thread(
            target=run_async_interval_test,
            args=(test_name, test_config, output_clients, sla_test_map)
        )
        test_thread.start()

        return test_thread

    except Exception as e:
        logging.error("Cannot start thread: %s" % e)
        sys.exit(1)


def config_logging(log_file):
    try:
        logging.basicConfig(
            filename=log_file,
            level=logging.INFO,
            format='[%(asctime)s][%(levelname)s][:%(name)s] %(message)s'
        )

    except Exception as e:
        logging.error("Cannot initialize logging in %s: %s" % (log_file, e))
        sys.exit(1)

def run_async_interval_test(test_name, test_config, output_clients, sla_test_map):
    """
    Runs a single test

    :param test_name:
    :param test_config:
    :param output_clients:
    :param sla_test_map:
    :return:
    """
    global stop_program

    while True:
        if stop_program is True:
            logging.info("[%s] exited" % test_name)
            sys.exit(0)

        interval = sla_test_map['interval']
        if "interval" in test_config.keys():
            interval = test_config["interval"]

        run_sync_test(test_name, test_config, output_clients, sla_test_map)

        logging.info("Sleeping period " + str(interval))
        for i in range(1, interval):
            if stop_program is True:
                logging.info("[%s] exited" % test_name)
                sys.exit(0)
            sleep(1)
    #todo write test outupt to outputs


def run_sync_test(test_name, test_config, output_clients, sla_test_map):
    """
    Run test in sync mode

    :param test_name:
    :param test_config:
    :param output_clients:
    :param sla_test_map:
    :return:
    """
    result = 1
    logs = ""
    test_context = str(uuid.uuid4())[0:6] + "-" + test_name
    logging.info(test_context + "Starting test with context " + test_context)
    # print(str(test_config))

    try:
        sla_test_map_copy = sla_test_map.copy()
        test_config_copy = test_config.copy()
        external_variables = sla_test_map_copy["parameters"]
        result, logs = execute_action(
            test_context,
            test_name,
            test_config_copy['action']['description'],
            test_config_copy['action'],
            test_config_copy['action']['action_type'],
            external_variables,
            sla_test_map_copy
        )
    except Exception as e:
        logging.error(test_context + "#1 There was an exception while running tests: " + str(e) + str(traceback.format_exc()))
        result = 1
    finally:
        logging.info("Writting SLA test result to output")
        write_sla_results(result, output_clients, test_config, test_context)

    return result, logs


def write_sla_results(result, outputs, test_config, test_context):
    """
    Write tests results to outputs, all threads wil wait here in order to use a single connection for outputs like influx
    Implemented like this to be thread safe

    :param result:
    :param outputs:
    :param action_specs:
    :return:
    """
    mutex_output.acquire()
    try:
        if "outputs" in test_config.keys():
            for output_name in test_config['outputs']:
                try:
                    if output_name not in outputs.keys():
                        raise ValueError("Can not write output, output " + output_name + " is not found")
                    logging.info("Writing result output to " + output_name)
                    outputs[output_name].write_result(result, test_config['test_name'], test_context)
                except Exception as e:
                    logging.error("There was an exception while writing output: " + str(traceback.format_exc()))
                finally:
                    logging.info("Write output done")
    finally:
        mutex_output.release()

def execute_action(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    """
    Runs a test action in a recursive mode

    :param description:
    :param test_name:
    :param action_specs:
    :param action_type:
    :param external_variables:
    :param sla_test_map:
    :return:
    """
    logging.info(test_context + " Execute action: " + description)
    result = None
    logs = ""

    if "time_parameters" in action_specs.keys():
        for time_var_name, time_var_def in action_specs["time_parameters"].items():
            logging.info(test_context + " Computing time parameter " + time_var_name)
            time_param_value = evaluate_time_parameter(time_var_def)
            external_variables[time_var_name] = time_param_value
            logging.info(test_context + " Computing time parameter as " + str(time_param_value))

    if action_type == "request":
        result, logs = sla_request(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "mysql_select":
        if "execute_on_remote_using_ssh" in action_specs:
            print("Execute in ssh context")
            packed_parameters = {
                "context": test_context,
                "action_specs": action_specs,
                "external_variables": external_variables,
                "dbal_connections": sla_test_map['dbal_conections']
            }
            message_bytes = json.dumps(packed_parameters).encode('ascii')
            base64_bytes = base64.b64encode(message_bytes)
            result, logs = sla_bin_remote_ssh(
                action_specs,
                external_variables,
                "/tmp/bunnyshell_automation_dist/sla_mysql",
                ["--test-env", base64_bytes.decode("utf-8") ]
            )
        else:
            result, logs = sla_mysql(
                test_context,
                action_specs,
                external_variables,
                sla_test_map['dbal_conections']
            )
            print("Executed mysql sla remote: " + str(result))
    elif action_type == "elasticsearch_query":
        result, logs = sla_elasticsearch_query(test_context,  description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssl_expiration_date":
        result, logs = sla_ssl_expiration(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "rabbitmq_get_api_queues":
        result, logs = sla_rabbitmq_api_queue(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)

    # generic vm checkpoints
    elif action_type == "ssh_remote_swap":
        result, logs = sla_ssh_remote_swap(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_remote_bunnyshell_agent":
        result, logs = sla_ssh_remote_bunnyshell_agent(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_remote_digital_ocean_agent":
        result, logs = sla_ssh_remote_digital_ocean_agent(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_uptime_more_then_x_days":
        result, logs = sla_ssh_uptime_more_then_x_days(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_log_rotation_enabled":
        result, logs = sla_ssh_log_rotation_enabled(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)

    # varnish metrics based on influx a with netdata
    elif action_type == "sla_varnish_influx_netdata_failed_backend_connections":
        result, logs = sla_varnish_influx_netdata_failed_backend_connections(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    else:
        raise ValueError(test_name + " -> ERR Action type " + action_type + " is not recognized")

    if result != 0:
        return result, logs

    if "action" in action_specs.keys():
        logging.info(test_context + " -> continue with next action in line")
        return execute_action(
            test_context,
            test_name,
            action_specs['action']['description'],
            action_specs['action'],
            action_specs['action']['action_type'],
            external_variables,
            sla_test_map
        )
    else:
        return result, logs
