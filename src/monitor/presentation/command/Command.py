from __future__ import annotations
from abc import ABC, abstractmethod
import argparse

class Command(ABC):
    """
    The Command interface declares a method for executing a command.
    """
    parser = None
    description = "perform a request and display header results"
    parameters = {}

    def __init__(self):
        pass

    def help(self):
        self.parser.print_help()

    @abstractmethod
    def execute(self) -> None:
        pass
