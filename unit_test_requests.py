from generator import generate_test_code_from_map
from docker_manager import restart_docker_containers
import sys
import json


map = {
    "header": {
        "hostname": "https://locust.io/",
        "domain": "locust.io",
        "hosts": {
            "locust.io.com": "127.0.0.1",
        },
    },
    "requests": {
        "home": {
            "type": "get",
            "url": "/",
            "headers": {
                "Content-type": "application/json"
            },
            "authorization": {
                "type": "api_key",
                "key": 'my-auth-key',
                "value": 'my-auth-value',
                "add_to": "header"
            },
            "allow_redirects": 1
        },
        "documentation": {
            "type": "get",
            "verify": 0,
            "url": "/en/stable/",
            "authorization": {
                "type": "bareer",
                "token": "<api token>"
            },
            "allow_redirects": 0
        },
        "product": {
            "type": "get",
            "url": "/en/stable/",
            "verify": 1,
            "authorization": {
                "type": "basic_auth",
                "username": "ionut",
                "password": "parola"
            }
        },
        "post_checkout_form_data": {
            "type": "post",
            "url": "/en/stable/",
            'form-data': {
                "t1": "v1",
                "t2": "v2"
            }
        },
        "post_checkout_x-www-form-urlencoded": {
            "type": "post",
            "url": "/en/stable/",
            'x-www-form-urlencoded': {
                "t1": "v1",
                "t2": "v2"
            }
        },
        "post_checkout_raw_json": {
            "type": "post",
            "url": "/en/stable/",
            'raw_json': {
                "t1": "v1",
                "t2": "v2"
            }
        },
        "post_checkout_raw_text": {
            "type": "post",
            "url": "/en/stable/",
            'raw_text': "jklfd kljfkldsjlkjkks",
            "pre_request_script": "time.sleep(300)",
            "variables": {
                "_csrf_token": {
                    "extract_type": "dom_attribute_value",
                    "attribute_name": "value",
                    "cssselector": "input[name='_csrf_token']"
                }
            }
        }
    },
    "users": {
        "user_basic": [
            {
                "type": "parallel",
                "tasks": [
                    {
                        "percent": 25,
                        "request": "home"
                    },
                    {
                        "percent": 25,
                        "request": "documentation"
                    },
                    {
                        "percent": 25,
                        "request": "product"
                    },
                    {
                        "percent": 25,
                        "request": "post_checkout_form_data"
                    },
                    {
                        "percent": 25,
                        "request": "post_checkout_x-www-form-urlencoded"
                    },
                    {
                        "percent": 25,
                        "request": "post_checkout_raw_json"
                    },
                    {
                        "percent": 25,
                        "request": "post_checkout_raw_text"
                    }
                ]
            }
        ]
    }
}


if __name__ == "__main__":
    # usage in cli
    docker_compose_file = 'compiled_docker_compose.yml'
    main_file = 'main.py'

    credentials_user = "ionut"
    credentials_password = "ionut2"
    # map_json = sys.argv[1]
    # credentials_user = sys.argv[2]
    # credentials_password = sys.argv[2]
    credentials = credentials_user + ":" + credentials_password

    # map = json.loads(map_json)

    print("Decoded map %s" % str(map))
    generate_test_code_from_map(map, mainFile=main_file, dockerComposeFile=docker_compose_file, credentials=credentials)
    # restart_docker_containers(dockerComposeFile='compiled_docker_compose.yml', slaveNumber=2)
