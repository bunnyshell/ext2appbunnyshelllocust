from src.get_request import  GetRequest
import logging
import sys
from jinja2 import Template
import json
import re
from src.variables_crapper import extract_variables, identify_external_variables


class ClassGenerator:

    def __init__(self):
        self.methods = []
        self.extends = ""
        self.name = "MainFlow"
        self.classes = []
        self.identation_level = 0
        self.annotations = []
        self.properties = {}

    def set_property(self, name, value):
        self.properties[name] = value

    def set_extends(self, extends):
        self.extends = extends

    def add_annotation(self, annotation):
        self.annotations.append(annotation)

    def add_method(self, name, annotations, body):

        self.methods.append({
            'name': name,
            'annotations': annotations,
            'body': body
        })

    def set_name(self, name):
        self.name = name

    def add_class(self, task_class_generator):
        task_class_generator.identation_level = self.identation_level + 1
        self.classes.append(task_class_generator)

    def generate(self, stress_map):
        f = open("templates/taskset_class_template.j2", "r")
        file_content = f.read()

        template = Template(file_content)

        classes_code = []
        for class_task in self.classes:
            classes_code.append(class_task.generate())


        ident_body = self.get_identiation(self.identation_level + 2)
        for method in self.methods:
            body_lines = method['body'].split("\n")
            reformat_body = ""
            for line in body_lines:
                reformat_body = reformat_body + ident_body + line + "\n"
            method['body'] = reformat_body

        extends = ":"
        if self.extends is not None and self.extends:
            extends = "(" + self.extends + "):"

        return template.render(
            stress_map=stress_map,
            properties=self.properties,
            extends=extends,
            classname=self.name,
            methods=self.methods,
            classes_code=classes_code,
            annotations=self.annotations,
            ident_class=self.get_identiation(self.identation_level),
            ident_method=self.get_identiation(self.identation_level + 1),
        )

    def get_identiation(sel, level):
        spaces = level * 4
        space_chars = ""
        for i in range(0, spaces):
            space_chars = space_chars + " "

        return space_chars


class LocustCodeGenerator:

    def __init__(self):
        self.classes = []
        self.array_vars = []
        self.user_class_names = []

    def add_user_class_names(self, user_class_name):
        self.user_class_names.append(user_class_name)

    def add_class(self, task_class_generator):
        # task_class_generator.identation_level = self.identation_level + 1
        self.classes.append(task_class_generator)

    # def add array_vars():

    def generate(self, stress_map):
        f = open("templates/locust_user_template.j2", "r")
        file_content = f.read()

        template = Template(file_content)

        classes_code = []
        for class_task in self.classes:
            classes_code.append(class_task.generate(stress_map=stress_map))

        return template.render(
            task_set_classes=classes_code,
            stress_map=stress_map,
            user_class_names=self.user_class_names
        )

def generate_code_main_flow(logger, stress_test_map):
    code_generator = LocustCodeGenerator()

    for user_name, behaviours in stress_test_map['users'].items():
        user_class = ClassGenerator()
        user_class_name = re.sub("[^0-9a-zA-Z]+", "_", user_name)
        user_class.set_name(user_class_name)
        user_class.set_extends("HttpUser")
        user_class.set_property("weight", "1000")
        user_class.set_property("min_wait", "ONE_MINUTE * 0.25")
        user_class.set_property("max_wait", "ONE_MINUTE")
        hostname = stress_test_map['header']['schema'] + "://" + stress_test_map['header']['domain']
        user_class.set_property("host", '"' + hostname.replace("\"", "\\\"") + '"')

        tasks = {}
        code_generator.add_user_class_names(user_class_name)

        logger.info("Compute user %s" % user_name)
        for behaviour in behaviours:
            if "name" not in behaviour:
                raise ValueError("Name not found in behaviour definition")
            task_set_class_name = re.sub("[^0-9a-zA-Z]+", "_", behaviour['name'])
            task_set_generator = ClassGenerator()
            task_set_generator.set_name(task_set_class_name)
            task_set_generator.set_extends("TaskSet")

            tasks[task_set_class_name] = 100

            if behaviour['type'] == 'parallel':
                logger.info("process parallel tasks")

                index = 0
                for task in behaviour['tasks']:
                    if "is_active" in task and task["is_active"] == 0:
                        logger.info("Ignore task, is not active " + str(task))
                        continue

                    index = index + 1
                    logger.info('Generate for task %s' % str(task))
                    #
                    # if "type" in task:
                    #     if not "config" in task:
                    #         raise ValueError("Config not found in task set")
                    #     index = index + 1
                    #     new_stress_map = stress_test_map
                    #     new_stress_map['users'] = task['config']
                    #     generated_body = generate_code_main_flow(logger, new_stress_map)
                    #     generated_body.set_name("MainFlow_" + str(index))
                    #     task_set_generator.add_class(generated_body)
                    #     continue

                    method_name = user_name + "_" + str(index) + "_" + re.sub("[^0-9a-zA-Z]+", "_", task['request'])
                    annotations = [ '@task(' + str(task['percent']) + ")" ]

                    body = "external_variables = {}\n"
                    task_request = stress_test_map['requests'][task['request']]
                    body = build_task_request(task_request, method_name, stress_test_map, body)

                    task_set_generator.add_method(method_name, annotations, body)

            code_generator.add_class(task_set_generator)
        user_class.set_property("tasks", json.dumps(tasks).replace("\"", ""))
        code_generator.add_class(user_class)
    return code_generator


def build_task_request(task_request, method_name, stress_test_map, body):
    # variables requests will be executed bofore any requests (pre or main request)
    if "variables" in task_request:
        task_variables = task_request['variables']
        body = build_variables(task_variables, stress_test_map, body)
    # pre requests will allwas be exected before main request
    if "pre_request_requests" in task_request:
        for pre_request in task_request['pre_request_requests']:
            request_name = pre_request['request']
            if request_name not in stress_test_map['requests']:
                raise ValueError("Wrong configuration, request not fond in stress map requests list")
            request_settings = stress_test_map['requests'][request_name]
            body = build_request(request_name, request_settings, body, stress_test_map)

    body = build_request(method_name, task_request, body, stress_test_map)
    if "post_variables" in task_request:
        task_variables = task_request['post_variables']
        body = build_variables(task_variables, stress_test_map, body)

    if "extractors" in task_request:
        task_variables = task_request['extractors']
        body = build_variables(task_variables, stress_test_map, body)

    # build post requests
    if "post_requests" in task_request:
        for post_request in task_request['post_requests']:
            # print(post_request['request'])
            # exit(33)
            request_name = post_request['request']
            if request_name == "":
                continue

            if request_name not in stress_test_map['requests']:
                raise ValueError("Wrong configuration, request not fond in stress map requests list")
            request_settings = stress_test_map['requests'][request_name]
            if "pre_request_script" in post_request:
                body = body + post_request['pre_request_script'] + "\n"

            body = build_task_request(request_settings, request_name, stress_test_map, body)

            if "variables" in post_request:
                body = build_variables(post_request['variables'], stress_test_map, body)

                # variables requests will be executed bofore any requests (pre or main request)

    return body

def build_variables(task_variables, stress_test_map, body):
    """
    Build variables

    :param task_variables:
    :param stress_test_map:
    :param body:
    :return:
    """
    extractor_options = ["first_value", "count_nodes", "nodes", "all_values", "from_existing_response"]
    extractor_vars = {}
    static_vars = {}
    for variable_name, variable_value in task_variables.items():
        is_extractor = False
        for extractor_option in extractor_options:
            if extractor_option in variable_value or extractor_option in variable_name:
                is_extractor=True
                break
        if is_extractor:
            extractor_vars[variable_name] = variable_value
        else:
            static_vars[variable_name] = variable_value

    if len(extractor_vars) > 0:
        body = body + "\nextractors = " + json.dumps(extractor_vars, indent=2)
        body = body + "\nexternal_variables = extract_variables(extractors=extractors, response=response, extracted_variables=external_variables)\n"
        body = body + "logging.info(\"External variables are:\" + str(external_variables))\n"

    if len(static_vars) > 0:
        body = body + "\nexternal_variables.update(" + json.dumps(task_variables, indent=2) + ")\n"
    # for extractor in task_variables:
    #     if "continue_condition" in extractor:
    #         if extractor['continue_condition'] == "not_empty_array":
    #             body = body + "if len(external_variables[\"" + extractor['name'] + "\"]) == 0:\n    logging.info(\"Can not continue, empty var list " + extractor['name'] + "\")\n    return\n"

    return body


def build_extract_variables(response_name="response"):
    request_body = "\n" \
        + "if " + response_name + ".status_code != 200:\n" \
        + '    raise ValueError("Status code is !== 200")\n' \
        + "external_variables = extract_variables(extractors=extractors, response=" + response_name +", extracted_variables=external_variables)\n" \
        + "logging.info(\"External variables are:\" + str(external_variables))\n"
    #continue_condition
    return request_body


def build_request(request_name, task_request, body, stress_test_map):
    body = body + "\n#-------------------------------------------\n"
    body = body + "#|      " + request_name + "\n"
    body = body + "#-------------------------------------------\n"
    verify_arg = 'True'
    if "verify" in task_request:
        verify_arg = str(bool(task_request['verify']))

    headers = {}
    if "headers" in task_request:
        headers = task_request['headers']

    timeout = 120
    if "timeout" in task_request:
        timeout = task_request['timeout']

    url = task_request['url']
    basic_auth_content = ""

    if "authorization" in task_request:
        authorization = task_request['authorization']
        if authorization['type'] == "api_key":
            auth_api_key_name = authorization['key']
            auth_api_key_value = authorization['value']
            auth_api_key_add_to = authorization['add_to']
            if auth_api_key_add_to == "header":
                headers[auth_api_key_name] = auth_api_key_value
            elif auth_api_key_add_to == "query_params":
                if "?" in url:
                    url = url + "&" + auth_api_key_name + "=" + auth_api_key_value
                else:
                    url = url + "?" + auth_api_key_name + "=" + auth_api_key_value
            else:
                raise ValueError("Authorization type can not set, unrecognized add_to")
        elif authorization['type'] == "bareer":
            token = authorization['token']
            headers['Authorization'] = "Bearer " + token
        elif authorization['type'] == "basic_auth":
            username = authorization['username']
            password = authorization['password']
            #todo esacpe "'" char
            basic_auth_content = ", auth=HTTPBasicAuth('" + username + "', '" + password + "')"
        else:
            raise ValueError("Authorization type not recognized")

    allow_redirects = 'False'
    if "allow_redirects" in task_request:
        allow_redirects = str(bool(task_request['allow_redirects']))

    files = []
    post_payload = "{}"
    if task_request['type'] == "post":
        if "form-binary" in task_request:
            post_payload = "build_webkit_form_boundaries(\n" + json.dumps(task_request['form-binary'], indent=2) + "\n)\n"
            headers['Content-Type'] = 'multipart/form-data; boundary=----WebKitFormBoundarysw7Xm6kf8wRcT7RX'
            files = []
        if 'form-data' in task_request:
            post_payload = json.dumps(task_request['form-data'], indent=2)
            files = []
        elif 'x-www-form-urlencoded' in task_request:
            form_elements = task_request['x-www-form-urlencoded']
            post_payload = []
            for v, k in form_elements.items():
                post_payload.append(str(v) + "=" + str(k))
            post_payload = "\"" + "&".join(post_payload).replace("\"", "\\\"") +  "\""
            headers['Content-Type'] = 'application/x-www-form-urlencoded'
            # post_payload = json.dumps(form_elements, indent=2)
            # post_payload = "\"" + json.dumps(form_elements).replace("\"", "\\\"") + "\""

        elif 'raw_json' in task_request:
            headers['Content-Type'] = 'application/json'
            post_payload = "\"" + json.dumps(task_request['raw_json']).replace("\"", "\\\"") + "\""

        elif 'raw_text' in task_request:
            headers['Content-Type'] = 'text/plain'
            post_payload = "\"" + task_request['raw_text'].replace("\"", "\\\"") + "\""

        elif 'raw_javascript' in task_request:
            headers['Content-Type'] = 'application/javascript'
            post_payload = "\"" + task_request['raw_javascript'].replace("\"", "\\\"") + "\""

        elif 'raw_html' in task_request:
            headers['Content-Type'] = 'text/html'
            post_payload = "\"" + task_request['raw_html'].replace("\"", "\\\"") + "\""

        elif 'raw_xml' in task_request:
            headers['Content-Type'] = 'application/xml'
            post_payload = "\"" + task_request['raw_xml'].replace("\"", "\\\"") + "\""

        post_payload = build_payload(post_payload)
    else:
        logging.info("check get request allows cache: ")
        if "cache_skip_with_query_parameter" in stress_test_map['header'] and stress_test_map['header']["cache_skip_with_query_parameter"]:
            logging.info("cache not allowed, introducing entropy")
            if "?" in url:
                url = (url + "&skipcacheautomation=\" + auto_random_int_16() + \"")
            else:
                url = (url + "?skipcacheautomation=\" + auto_random_int_16() + \"")

    body = body + "\nheaders = " + json.dumps(headers, indent=2) + "\n"
    body = body + "payload = " + post_payload + "\n"
    body = body + "files = []" + "\n"

    pre_request_script = ""
    if "pre_request_script" in task_request:
        pre_request_script = task_request['pre_request_script']
    body = body + pre_request_script + "\n"
    body = body + "logging.info(\"Execute request:" + request_name + "\")\n"
    body = body + "logging.info(\"Payload is:\" + str(payload))\n"


    body = body \
           + 'response = self.client.' + task_request['type'] + '(' \
           + "identify_external_variables(\"" + url + "\", external_variables_name=\"external_variables\")" \
           + ', verify=' + verify_arg \
           + ', headers=headers, timeout=' + str(timeout) \
           + basic_auth_content \
           + ", allow_redirects=" + allow_redirects
    if task_request['type'] == "post":
        #", payload=payload"
        body = body +  ", data=payload" \
           + ", files=files" \
           + ')'
    else:
        body = body + ")"

    body = body + "\nlogging.info(\"--> REQUEST TOOLBELT DEBUG:\")\n"
    body = body + "\ndata_resp = dump.dump_all(response)\n"
    body = body + "\nprint(str(data_resp))\n"
    body = body + "\nlogging.info(\"Response headers:\" + str(response.headers))\n"
    body = body + "\nlogging.info(\"Response status code:\" + str(response.status_code))\n"
    body = body + "logging.info(\"COOKIE JAR: \" + str(self.client.cookies))\n"

    return body


def build_payload(payload):
    new_paylod = {}
    if isinstance(payload, str):
        return identify_external_variables(payload, external_variables_name="external_variables")

    for key, value in payload.items():
        if isinstance(value, str):
            new_paylod[key] = identify_external_variables(value, external_variables_name="external_variables")
        else:
            new_paylod[key] = value
    return new_paylod


def generate_url_definition(url):
    # url = identify_external_variables(url, external_variables_name="external_variables")

    return url