import gevent
from locust import HttpUser, task, between
from locust.env import Environment
from locust.stats import stats_printer, stats_writer, stats_history_csv_header, write_csv_files
from locust.log import setup_logging
import locust.stats
from locust import log
import logging
from locust import events, runners
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
import time
from locust.stats import RequestStats, chain, sort_stats, PERCENTILES_TO_REPORT
from locust.runners import LocalRunner
import time
import json
from api import update_stress_test_run

test_tracking = {
    'failers': [],
    'requests': {}
}


def track_current(stats, environment):
    print("TRACK CURRENT......")
    point_key = int(time.time())

    requests_dict = requests_stats_to_json(stats, environment)
    failers_dict = failures_to_json(stats, environment)

    test_tracking["requests"][point_key] = requests_dict

    test_tracking['failers'] = failers_dict

    j = json.dumps(test_tracking, indent=4)
    print(j)


def failures_to_json(stats, environment):
    """"Return the contents of the 'failures' as dict object."""
    rows = []

    for s in sort_stats(stats.errors):
        rows.append({
            "Method": s.method,
            "Name": s.name,
            "Error": s.error,
            "Occurrences": s.occurrences,
        })

    return rows


def requests_stats_to_json(stats, environment):

    """ Returns a list of dicts which represents data about each request entry """
    rows = []

    for s in chain(sort_stats(stats.entries), [stats.total]):
        if s.num_requests:
            percentiles = [str(int(s.get_response_time_percentile(x) or 0)) for x in PERCENTILES_TO_REPORT]
        else:
            percentiles = ["0"] * len(PERCENTILES_TO_REPORT)

        formatter_stats = {
            "Type": s.method,
            "Name": s.name,
            "Uer Count": environment.runner.user_count,
            "Request Count": s.num_requests,
            "Failure Count": s.num_failures,
            "Median Response Time": s.median_response_time,
            "Average Response Time": s.avg_response_time,
            "Min Response Time": s.min_response_time or 0,
            "Max Response Time": s.max_response_time,
            "Average Content Size": s.avg_content_length,
            "Requests/s": s.total_rps,
            "Failures/s": s.total_fail_per_sec,
            "50%": percentiles[0],
            "66%": percentiles[1],
            "75%": percentiles[2],
            "80%": percentiles[3],
            "90%": percentiles[4],
            "95%": percentiles[5],
            "98%": percentiles[6],
            "99%": percentiles[7],
            "99.9%": percentiles[8],
            "99.99%": percentiles[9],
            "99.999%": percentiles[10],
            "100%": percentiles[11],
        }

        rows.append(formatter_stats)

    return rows

def save_tracking(stress_test_id, stress_test_run_id, organization_id):
    logging.info("Save test results...")
    update_stress_test_run(
        {
            'stress_test_id': stress_test_id,
            'stress_test_run_id': stress_test_run_id,
            'organization_id': organization_id,
            'status': "executed_ok",
            'result': json.dumps(test_tracking)
        }
    )